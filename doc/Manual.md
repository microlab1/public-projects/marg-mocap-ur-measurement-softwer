# Measurement software Manual

To launch the measurement sowtware start the [master/src](https://gitlab.com/microlab1/public-projects/marg-mocap-ur-measurement-softwer/-/tree/master/src)/Marg_mod.py file.

The starting window will be the following:

<img align="center" src="doc/pics/main_window.png" width=90%>

## **MARG**

In the left panel the values of the MARG sensor can be set and observed.
* Checkbox: after connecting the MARG sensor via USB check the box. Successful connection is diyplayeb by green Connected status.
* Actual sample time: Feedback of the sampling time current setting
* Sampling time: New sampling time in ms can be setted after typing in a new value and clicking on the **Set** button. 
* Acc, Gyro and Mag sensor values displayed in sensor coordinate system

## **MoCap**

In the middle panel the values of the MoCap system can be set and observed.

Make sure that the laptop used during the measurement and the MoCap system are connected to the same WiFi network. Check the MoCap streaming IP in the  [master/src](https://gitlab.com/microlab1/public-projects/marg-mocap-ur-measurement-softwer/-/tree/master/src)/NatNetClient.py. 
* Checkbox:  after connecting the MoCap system via WiFi check the box. Successful connection is diyplayeb by green Connected status.
* Bodies: the defined bodies in the MoCap system will be displayed (max 5). Checking the checkboxes means that the body will be loged in the measurement file. Multiple bodies can be chosen as well. After clicking on button of a body the streaming info of the choosen bodi will be displayed.
* Selected body: displays the selected body for observation
* Qvaternion: real time rotation of the selected body in quaternion form 
* Euler: real time rotation of the selected body in Euler form
* Position: real time position data of the selected body 

## **Robot**

In the right panel the values of the MoCap system can be set and observed.

* Checkbox: after connecting the UR3 robot via RJ45 check the box. Successful connection is diyplayeb by green Connected status.
* Robot IP: set the robot IP for remote control
* Config: set a file containing a desired robot path. Example file is available in [master/src/config](https://gitlab.com/microlab1/public-projects/marg-mocap-ur-measurement-softwer/-/tree/master/src/config) folder.
* Parameter setting: radius, velocity and acceleration of the robot during the measurement.
    * Depending on the choosen option Min-Max values or a constant value can be set
    * Options:
        * Linear: min max range is divided according to the epoch number 
        * Constant: only van value
        * Mesh grid: depending on the parameter setting the program generate a full meshgrid and provide different setups for each epoch
        * GaussRandom: mu and std values are required. Set a different setup in each epoch. 
        * UniformRandom: min and max values are required. Set a different setup in each epoch. 
* Noise settings:
    * tan: noise on the tangencial axis
    * norm: noise on the normal axis
    * binorm: noise on the binormal axis
    * Options:
        * Uniform: niform distribution
        * Normal: normal distribution
* Epochs: desired epoch number. (In MeshGrid mode only applies for one parameter)
* Load program: Generates the full movement program for the robot. Upload the program to the robot and sets the robot into the starting position.
* Move Robot: starts the robot movement program
* Stop Robot: stops the robot movements

## **Logger**

In the lower panel the logger parameters can be set.

* Path: path of the saved log files
* Status: current status of the logger
* Start Log: start the measurement
* Stop Log: stop the measurement

## **Measurement file**

Example measurement can be found in [master/log](https://gitlab.com/microlab1/public-projects/marg-mocap-ur-measurement-softwer/-/tree/master/log)
* Marg.csv: loged data from MARG sensor
* Mocap.csv: loged data from the MoCap system
* robotprog.txt: full generated robot program for the measurement with parameter list at the begining of the file
