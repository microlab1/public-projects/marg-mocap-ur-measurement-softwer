<img src="/doc/pics/GPK_BME_MOGI.png">

# MARG-MoCap-UR measurement softwer

This project contains a measurement software which includes the MARG sensor, the Motive Motion Capture software and the UR3 robot. Integrating this 3 main parts it is able to log combined measurements. The softwer implemented in Python.

[[_TOC_]]

## Implemented by<br>
Balázs Nagy <nagybalazs@mogi.bme.hu> <br>
Natababara Gyöngyössy <natabara@gyongyossy.hu>

## Source and references <br>
This code includes
Phidget API available at: <br>
https://www.phidgets.com/?tier=3&prodid=48 <br>
NatNet SDK available at: <br> 
https://optitrack.com/downloads/developer-tools.html#natnet-sdk

## Installation guide<br>
**1. step:** Clone this repository. <br>
The repository contains the before mentioned modified NatNet SDK and an Anaconda environment file (**env/environment.yml**). <br>
**2. step:** Set up the environmemnt. <br>
All of the nessecary Python packages are listed in the envirenment file. If the Phidget package does not install properly a manual pip installation is required.<br>
```
pip install Phidget22
```
**3. step:** Launch the **src/MARG_mod.py** file <br>
After a proper setup the code will run. In Spyder IDE run it in external terminal.
 

Example measurement files: <br>
[/master/log](https://gitlab.com/microlab1/public-projects/marg-mocap-ur-measurement-softwer/-/tree/master/log)

AHRS filter: <br>
https://github.com/Mayitzin/ahrs

Usefull:  <br>
https://x-io.co.uk/open-source-imu-and-ahrs-algorithms/

## Manual<br>

The manual for the program is in the [/master/doc](https://gitlab.com/microlab1/public-projects/marg-mocap-ur-measurement-softwer/-/tree/master/doc) folder.

## Known bugs<br>
- sometimes MoCap does not connect properly. In this case make sure that the MoCap system is streaming, the IP is matching and the measurement device is on the same WiFi as the MoCap. If the robot is connected via RJ45 unplug it. Connect the MoCap system and replug the robot. <br>

- If the robot is switched to manual control while connected to the measurement program the program will crash. Recommended to unconnect the robot in the measurement program before switch the control to manul. After the nessesary adjustments set the robot back to remote control and measurement program can reconnect to it. 