from queue import Queue

class MeasurementDevice:
    def __init__(self):
        self.outputQueue = Queue()
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def configure(self):
        pass

    def onIncomingData(self):
        pass

    def onError(self):
        pass