import socket
import time

class URCom:
    def __init__(self,ip,port):
        self.ip=ip
        self.port=port
        self.buffsize=2048
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.settimeout(5)
        self.isconnected=False

    def conn(self):
        try:
            self.s.connect((self.ip, self.port),)
        except:
            raise Exception("Nincs robot")
        self.isconnected = True

    def recv(self):
        adat=self.s.recv(self.buffsize)
        self.lastrec=time.time()
        return adat

    def send(self,data):
        self.s.send(data.encode("UTF-8"))

    def sendbinary(self,data):
        self.s.send(data)

    def close(self):
        self.s.close()
        self.isconnected = False