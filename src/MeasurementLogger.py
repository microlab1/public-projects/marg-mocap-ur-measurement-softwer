from threading import Thread
from csv import DictWriter
from datetime import datetime
import os
import shutil
from queue import Empty

class Logger:
    def __init__(self):
        self.deviceQueues=dict()
        self.logThreads=dict()
        self.stopRequested=False
        self.logpath="./"

    def prepareLogPath(self,path):
        timestamp = datetime.now().strftime("%Y%m%d_%H_%M_%S")
        if path[-1]!="/":
            path.append("/")
        self.logpath = path+timestamp+"/"
        if os.path.exists(self.logpath):
            shutil.rmtree(self.logpath)
            os.makedirs(self.logpath, exist_ok=True)
        else:
            os.makedirs(self.logpath,exist_ok=True)


    def setDevices(self,deviceDict,clearOldDevices=True):
        if clearOldDevices:
            self.deviceQueues.clear()
            self.logThreads.clear()
        for k,v in deviceDict.items():
            self.deviceQueues[k]=v.outputQueue
            self.logThreads[k]=Thread(target=self.logFunction,args=(v.outputQueue,v.getFieldNames(),k))

    def startLogging(self):
        self.stopRequested=False
        for k,v in self.deviceQueues.items():
            v.maxsize=0
            self.logThreads[k].start()

    def stopLogging(self):
        self.stopRequested=True
        for k,v in self.logThreads.items():
            v.join()
            self.deviceQueues[k].maxsize=1

    def logFunction(self,outputQueue,fieldNames,deviceName):
        print(fieldNames)
        with open(self.logpath+deviceName+".csv","w",newline='') as f:
            writer = DictWriter(f,fieldnames=fieldNames)
            writer.writeheader()
            #Read the first (old) entry
            try:
                outputQueue.get_nowait()
            except Empty:
                pass
            while not self.stopRequested:
                try:
                    writer.writerow(outputQueue.get(timeout=0.1))
                except Empty:
                    pass