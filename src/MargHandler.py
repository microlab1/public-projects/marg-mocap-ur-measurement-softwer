from queue import Queue, Empty, Full
from Phidget22.Phidget import *
from Phidget22.Devices.Spatial import *
import time

class MARG:
    def __init__(self):
        self.outputQueue = Queue()
        self.monitorQueue = Queue()
        self.spatial = Spatial()
        self.spatial.setOnSpatialDataHandler(self.onIncomingData)
        self.spatial.setOnErrorHandler(self.onError)
        self.lastData={}
        self.timeOffset=0

    def start(self):
        self.spatial.openWaitForAttachment(5000)
        self.timeOffset = time.time()
        self.spatial.setDataInterval(4)
        self.outputQueue.maxsize=1
        self.monitorQueue.maxsize = 1

    def setSampleTime(self,sampleTime):
        self.spatial.setDataInterval(sampleTime)

    def getSampleTime(self):
        return str(self.spatial.getDataInterval())

    def monitorReadOne(self):
        try:
            return self.monitorQueue.get(timeout=0.01)
        except Empty:
            return None

    def stop(self):
        self.spatial.close()

    def configure(self):
        pass

    def onIncomingData(self, device, acceleration, angularRate, magneticField, timestamp):
        datadict = {"Timestamp":timestamp/1000+self.timeOffset,"AccX":acceleration[0],"AccY":acceleration[1],"AccZ":acceleration[2],
                              "GyroX":angularRate[0],"GyroY":angularRate[1],"GyroZ":angularRate[2],
                              "MagX":magneticField[0], "MagY":magneticField[1], "MagZ":magneticField[2]}
        try:
            self.outputQueue.put_nowait(datadict)
        except Full:
            pass

        datadict.update({"MagX": datadict["MagX"] if datadict["MagX"] < 1e290 else self.lastData["MagX"],
                         "MagY": datadict["MagY"] if datadict["MagY"] < 1e290 else self.lastData["MagY"],
                         "MagZ": datadict["MagZ"] if datadict["MagZ"] < 1e290 else self.lastData["MagZ"], })
        try:
            self.lastData = datadict
            self.monitorQueue.put_nowait(datadict)
        except Full:
            pass

    def getFieldNames(self):
        return  ["Timestamp","AccX","AccY","AccZ","GyroX","GyroY","GyroZ","MagX","MagY","MagZ"]

    def onError(self):
        pass