import numpy as np
from URconnect import URCom
import socket
from threading import Thread
import time
from queue import Queue
import json

class Robot:
    def __init__(self,robotip):
        self.robot = URCom(robotip, 30002)
        self.program = ""
        self.path=[]
        self.epochnum=0

        self.randData={}
        self.kineData={}

        self.randomizer=None
        randomizers = [NormalRandomizer(),UniformRandomizer()]
        self.randomizerDict={}
        for randomizer in randomizers:
            self.randomizerDict[randomizer.getOptionInfo()[0]]=randomizer

        self.paramStepper=None
        paramSteppers = [LinearStepping(),MeshgridStepping(),ConstantStepping(),GaussRandomStepping(),UniformRandomStepping()]
        self.paramStepperDict={}
        for paramStepper in paramSteppers:
            self.paramStepperDict[paramStepper.getOptionInfo()[0]]=paramStepper

    def start(self):
        self.program = ""
        try:
            self.robot.conn()
        except:
            raise Exception("Nincs robot")

    def setConfig(self, pathFilePath, randData, kineData):
            try:
                self.randData=randData
                self.kineData=kineData
                self.path = []
                with open(pathFilePath,"r") as f:
                    lines = f.readlines()
                    if len(lines)<2:
                        return False
                    for line in lines:
                        rawpoint = np.array([float(x) for x in line.split(",")])
                        rawpoint[:3]/=1000
                        self.path.append(rawpoint)
                    self.randomizer = self.randomizerDict[randData["Type"]]
                    self.randomizer.configure(randData["Param"])

                    self.paramStepper = self.paramStepperDict[kineData["Type"]]
                    self.paramStepper.configure(kineData["Min"],kineData["Max"],kineData["Epochnum"])

                    return True
            except:
                return False

    def loadProgram(self, pathFilePath, randData, kineData):
        if self.setConfig(pathFilePath, randData, kineData):
            self.generateHeader()
            maxepoch = self.paramStepper.getEpoch()
            self.kineData["Epochnum"]=maxepoch
            for i in range(maxepoch):
                r,a,v = self.paramStepper.run(i)
                for j in range(len(self.path)):
                    self.generateDirections(j)
                    t,n,b = self.randomizer.run()
                    self.step(self.path[j],self.path[(j+1)%len(self.path)],t,n,b,r,a,v)
            self.generateFooter()
            self.sendHome()
            return True
        else:
            return False

    def sendHome(self):
        self.robot.send("\tmovej(p["+",".join([str(x) for x in self.path[0]])+"],a="+str(1)+",v="+str(1)+",r="+str(0)+")\n")

    def startProgram(self):
        print(self.program)
        self.robot.send(self.program)

    def generateHeader(self):
        self.program="def myprog():\n"

    def generateFooter(self):
        self.program+="end\n"

    def generateMidPoint(self,pStart,pEnd,t,n,b):
        pMid = pStart.copy()
        pMid[:3] = (pStart[:3]+pEnd[:3])/2
        pMid[:3] += np.linalg.norm(pStart[:3]-pEnd[:3])*t*self.tVec\
                  +np.linalg.norm(pStart[:3]-pEnd[:3])*n*self.nVec\
                  +np.linalg.norm(pStart[:3]-pEnd[:3])*b*self.bVec
        return pMid

    def step(self,pStart,pEnd,t,n,b,r,a,v):
        #self.moveL(pStart,r,a,v)
        self.moveL(self.generateMidPoint(pStart,pEnd,t,n,b),r,a,v)
        self.moveL(pEnd,r,a,v)

    def generateDirections(self,i):
        if len(self.path)==2:
            #Origo as a third point
            self.tVec = (self.path[1][:3]-self.path[0][:3])/np.linalg.norm(self.path[1][:3]-self.path[0][:3])
            self.nVec = np.cross(self.path[1][:3],self.path[0][:3])
            self.nVec = self.nVec/np.linalg.norm(self.nVec)
            self.bVec = np.cross(self.tVec,self.nVec)
            self.bVec = self.bVec / np.linalg.norm(self.bVec)
        else:
            preP = self.path[i-1][:3]
            P = self.path[i][:3]
            postP = self.path[(i+1)%len(self.path)][:3]
            self.tVec = (postP-P)/np.linalg.norm(postP-P)
            self.nVec = np.cross(P-preP,postP-P)
            self.nVec = self.nVec / np.linalg.norm(self.nVec)
            self.bVec = np.cross(self.tVec, self.nVec)
            self.bVec = self.bVec / np.linalg.norm(self.bVec)

    def moveL(self,pos,r,a,v):
        self.program+="\tmovel(p["+",".join([str(np.round(x,4)) for x in pos])+"],a="+str(np.round(a,4))+\
                      ",v="+str(np.round(v,4))+",r="+str(np.round(r,4))+")\n"

    def stop(self):
        self.stopProgram()
        self.robot.close()

    def stopProgram(self):
        self.robot.send("stopl(1)\n")

    def getKineMethods(self):
        return self.paramStepperDict.keys()

    def getRandMethods(self):
        return self.randomizerDict.keys()

    def getCurrentEpoch(self):
        return self.epochnum

    def logProgram(self,path):
        with open(path+"robotprog.txt","w") as f:
            f.write("Randomizer parameters: "+json.dumps(self.randData)+"\nKinematic parameters: "+json.dumps(self.kineData)\
                    +"\n******************\n")
            f.write(self.program)

class LinearStepping:
    def __init__(self):
        self.mins=[]
        self.maxs=[]
        self.epochnum=[]

    def configure(self,mins,maxs,epochnum):
        self.mins = mins
        self.maxs = maxs
        self.epochnum = epochnum
        self.vals = np.linspace(mins,maxs,epochnum)

    def run(self,i):
        return self.vals[i,:]

    def getEpoch(self):
        return self.epochnum

    def getOptionInfo(self):
        #Second_Row_Disabled? Label1 Label2
        return "Linear",False, "Min", "Max"

class ConstantStepping:
    def __init__(self):
        self.constants=[]
        self.epochnum=[]

    def configure(self,values,dummy,epochnum):
        self.constants = values
        self.epochnum = epochnum

    def run(self,i):
        return self.constants

    def getEpoch(self):
        return self.epochnum

    def getOptionInfo(self):
        #Second_Row_Disabled? Label1 Label2
        return "Constant",True, "Const", "None"

class UniformRandomStepping:
    def __init__(self):
        self.mins=[]
        self.maxs=[]
        self.epochnum=[]

    def configure(self,mins,maxs,epochnum):
        self.mins = mins
        self.maxs = maxs
        self.epochnum = epochnum
        self.vals = np.random.uniform(mins,maxs,epochnum)

    def run(self,i):
        return self.vals[i,:]

    def getEpoch(self):
        return self.epochnum

    def getOptionInfo(self):
        #Second_Row_Disabled? Label1 Label2
        return "UniformRandom",False, "Min", "Max"

class GaussRandomStepping:
    def __init__(self):
        self.mus=[]
        self.sigmas=[]
        self.epochnum=[]

    def configure(self,mus,sigmas,epochnum):
        self.mus = mus
        self.sigmas = sigmas
        self.epochnum = epochnum
        self.vals = np.random.normal(mus,sigmas,epochnum)

    def run(self,i):
        return self.vals[i,:]

    def getEpoch(self):
        return self.epochnum

    def getOptionInfo(self):
        #Second_Row_Disabled? Label1 Label2
        return "GaussRandom",False, "Mu", "Sigma"


class MeshgridStepping:
    def __init__(self):
        self.mins=[]
        self.maxs=[]
        self.epochnum=[]

    def configure(self,mins,maxs,epochnum):
        self.mins = mins
        self.maxs = maxs
        self.epochnum = epochnum
        vects = []
        for i in range(len(mins)):
            vects.append(np.linspace(mins[i],maxs[i],epochnum))
        self.vals=np.array(np.meshgrid(*vects)).reshape(len(mins),-1)

    def run(self,i):
        return self.vals[:,i]

    def getEpoch(self):
        return self.vals[0,:].flatten().shape[0]

    def getOptionInfo(self):
        #Second_Row_Disabled? Label1 Label2
        return "Meshgrid",False, "Min", "Max"

class UniformRandomizer:
    def __init__(self):
        self.widths = []

    def configure(self,params):
        self.widths=params

    def run(self):
        return [np.random.uniform(-w,w) for w in self.widths]

    def getOptionInfo(self):
        return "Uniform","MaxAbsDifference"

class NormalRandomizer:
    def __init__(self):
        self.widths = []

    def configure(self,params):
        self.widths=params

    def run(self):
        return [np.random.normal(0,w) for w in self.widths]

    def getOptionInfo(self):
        return "Normal","Deviation"