from NatNetClient import NatNetClient
import socket
import numpy as np
from queue import Queue, Empty, Full
import time
from collections import OrderedDict
from scipy.spatial.transform import Rotation as R
from itertools import permutations



class MoCap:
    def __init__(self):
        self.outputQueue = Queue()
        self.monitorQueue = Queue()
        # WARNING! AUTOMATIC IP DETECTION BASED ON A DUMMY RULE, TAKE CARE!
        myip = socket.gethostbyname(socket.gethostname())
        for IP in [i[4][0] for i in socket.getaddrinfo(socket.gethostname(), None)]:
            if IP.find("192.168.0.")>=0:
                myip = IP
        self.streamingClient = NatNetClient(server_ip_address="192.168.100.105", local_ip_address=myip)
        self.streamingClient.rigid_body_description_listener = self.registerRigidBody
        self.streamingClient.rigid_body_listener = self.onIncomingData
        self.streamingClient.new_frame_listener=self.newFrame
        self.bodyDict={}
        self.bodyNameList=[]
        self.trackedName = None
        self.fieldNames = ["Timestamp"]

    def start(self):
        self.frameCnt = None
        self.lastData={}
        self.outputQueue.maxsize=1
        self.monitorQueue.maxsize=1
        self.streamingClient.run()
        self.Timestamp=time.time()

    def stop(self):
        self.streamingClient.stopit()

    def setActiveBody(self,name):
        if name not in self.bodyNameList:
            self.bodyNameList.append(name)
            self.fieldNames+=self.genFieldNameList(name)

    def setInactiveBody(self,name):
        if name in self.bodyNameList:
            self.bodyNameList.remove(name)
            self.fieldNames = [x for x in self.fieldNames if x not in self.genFieldNameList(name)]

    def genFieldNameList(self,name):
        return [name+"_posX",name+"_posY",name+"_posZ",name+"_quatX",name+"_quatY",name+"_quatZ",name+"_quatW",
                name+"_rotX",name+"_rotY",name+"_rotZ"]

    def getFieldNames(self):
        return self.fieldNames

    def listRigidBodies(self):
        time.sleep(0.02)
        c = OrderedDict(sorted(self.bodyDict.items()))
        return list(c.items())

    def registerRigidBody(self,id,name):
        if id not in self.bodyDict.keys():
            self.bodyDict[id]=name.decode('utf-8')
        print(self.bodyDict)

    def newFrame(self,frame_number, marker_set_count, unlabeled_markers_count, rigid_body_count,
                                    skeleton_count, labeled_marker_count, time_code, time_code_sub, timestamp,
                                    is_recording, tracked_models_changed):
        if self.frameCnt is not None:
            try:
                self.outputQueue.put(self.lastData,timeout=0.1)
            except Full:
                pass
        self.Timestamp = time.time()
        self.lastData = {"Timestamp": self.Timestamp}
        self.frameCnt = frame_number

    def onIncomingData(self, framenum, timestamp, id, position, rot):
        if id in self.bodyDict.keys():
            euler = R.from_quat(rot).as_euler("zyx", degrees=True)[::-1]
            pos = []
            for coord in position:
                pos.append(coord*1000)

            name = self.bodyDict[id]
            if name == self.trackedName:
                try:
                    self.monitorQueue.put_nowait(list(pos)+list(rot)+list(euler))
                except Full:
                    pass
            if name in self.bodyNameList:
                self.lastData.update({name+"_posX":pos[0],name+"_posY":pos[1],name+"_posZ":pos[2],
                                      name+"_quatX":rot[0],name+"_quatY":rot[1],name+"_quatZ":rot[2],name+"_quatW":rot[3],
                                      name+"_rotX":euler[0],name+"_rotY":euler[1],name+"_rotZ":euler[2]})

    def setTrackedBody(self,name):
        self.trackedName=name

    def monitorReadOne(self):
        try:
            return self.monitorQueue.get(timeout=0.01)
        except Empty:
            return None

    def onError(self):
        pass

