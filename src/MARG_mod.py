# -*- coding: utf-8 -*-

# Imports
import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.checkbox import CheckBox
from MargHandler import MARG
from MocapHandler import MoCap
from RobotHandler import Robot
from MeasurementLogger import Logger

import time

class Logger_window(Widget):
    pass

class Logger_app(App):
    def build(self):
        self.mocapOn  = False
        self.margOn   = False
        self.robotOn  = False
        self.loggerOn = False
        Clock.schedule_interval(self.updateLabels,0.5)
        self.marg  = None
        self.robot = None
        self.mocap = None

        self.logger = Logger()

        self.layout = Logger_window()
        
        # Button and checkbox bindings with the GUI interface
        self.layout.ids.MargCheckbox_nodisable.bind(active=self.MargCheckbox_callback)
        self.layout.ids.MocapCheckbox_nodisable.bind(active=self.MocapCheckbox_callback)
        self.layout.ids.RobotCheckbox_nodisable.bind(active=self.RobotCheckbox_callback)
        self.layout.ids.SampletimeSet.bind(on_release=self.SetSampleTime_callback)
        self.layout.ids.LoadProgram.bind(on_release=self.LoadProgram_callback)
        self.layout.ids.MoveRobot.bind(on_release=self.MoveRobot_callback)
        self.layout.ids.StopRobot.bind(on_release=self.StopRobot_callback)
        self.layout.ids.SpinnerParam.bind(text=self.SpinnerParam_callback)
        self.layout.ids.SpinnerNoise.bind(text=self.SpinnerNoise_callback)
        self.layout.ids.StartButton.bind(on_release=self.StartButton_callback)
        self.layout.ids.StopButton.bind(on_release=self.StopButton_callback)

        self.layout.ids.Body1Check.bind(active=self.BodyCheck_callback)
        self.layout.ids.Body2Check.bind(active=self.BodyCheck_callback)
        self.layout.ids.Body3Check.bind(active=self.BodyCheck_callback)
        self.layout.ids.Body4Check.bind(active=self.BodyCheck_callback)
        self.layout.ids.Body5Check.bind(active=self.BodyCheck_callback)

        self.set_disable_all_children(self.layout.ids.Mocap,True)
        self.set_disable_all_children(self.layout.ids.Marg, True)
        self.set_disable_all_children(self.layout.ids.Robot, True)

        return self.layout

    def BodyCheck_callback(self,checkbox,value):
        buttonID=list(self.layout.ids.keys())[list(self.layout.ids.values()).index(checkbox)].replace("Check","")
        if value:
            self.mocap.setActiveBody(self.layout.ids[buttonID].text)
        else:
            self.mocap.setInactiveBody(self.layout.ids[buttonID].text)

    def initMocapBodies(self):
        rigidList = self.mocap.listRigidBodies()
        for i in range(5):
            if i < len(rigidList):
                self.layout.ids["Body" + str(i + 1)].text = rigidList[i][1]
                self.set_disable_all_children(self.layout.ids["Body" + str(i + 1)], False)
                self.set_disable_all_children(self.layout.ids["Body" + str(i + 1)+"Check"], False)
            else:
                self.layout.ids["Body" + str(i + 1)].text = "-"
                self.set_disable_all_children(self.layout.ids["Body" + str(i + 1)], True)
                self.set_disable_all_children(self.layout.ids["Body" + str(i + 1)+"Check"], True)

    def MocapCheckbox_callback(self, checkbox, value):
        if value:
            self.layout.ids.MocapStatus.text="Connecting..."
            self.mocap = MoCap()
            self.layout.ids.TrackedBody.text="None"
            try:
                self.mocap.start()
            except:
                self.layout.ids.MocapStatus.text = "Device not found"
                self.layout.ids.MocapStatus.color = (1, 0, 0, 1)
                checkbox.active = False
                return None

            self.mocapOn = True
            self.layout.ids.MocapStatus.text = "Connected"
            self.set_disable_all_children(self.layout.ids.Mocap,False)
            self.layout.ids.MocapStatus.color = (0,1,0, 1)
            self.initMocapBodies()
        else:
            self.layout.ids.MocapStatus.text = "Disconnecting..."
            if self.mocapOn:
                self.layout.ids.MocapStatus.color = (1, 1, 0, 1)
                self.mocap.stop()
            self.mocapOn = False
            self.layout.ids.MocapStatus.text = "Disconnected"
            self.set_disable_all_children(self.layout.ids.Mocap, True)

    def RobotCheckbox_callback(self, checkbox, value):
        if value:
            self.layout.ids.RobotStatus.text="Connecting..."
            self.robot = Robot(self.layout.ids.RobotIP_nodisable.text)
            try:
                self.robot.start()
                self.layout.ids.SpinnerParam.values = self.robot.getKineMethods()
                self.layout.ids.SpinnerParam.text = self.layout.ids.SpinnerParam.values[0]
                self.layout.ids.SpinnerNoise.values = self.robot.getRandMethods()
                self.layout.ids.SpinnerNoise.text = self.layout.ids.SpinnerNoise.values[0]

            except:
                self.layout.ids.RobotStatus.text = "Device not found."
                self.layout.ids.RobotStatus.color = (1, 0, 0, 1)
                checkbox.active=False
                return None

            self.robotOn = True
            self.layout.ids.RobotStatus.text = "Connected"
            self.layout.ids.RobotStatus.color = (0, 1, 0, 1)
            self.set_disable_all_children(self.layout.ids.Robot,False)
        else:
            self.layout.ids.RobotStatus.text = "Disconnecting..."
            if self.robotOn:
                self.layout.ids.RobotStatus.color = (1, 1, 0, 1)
                self.robot.stop()
            self.robotOn = False
            self.layout.ids.RobotStatus.text = "Disconnected"
            self.set_disable_all_children(self.layout.ids.Robot, True)

    def MargCheckbox_callback(self, checkbox, value):
        if value:
            self.layout.ids.MargStatus.text="Connecting..."
            self.marg = MARG()
            try:
                self.marg.start()
            except:
                self.layout.ids.MargStatus.text = "Device not found"
                self.layout.ids.MargStatus.color=(1,0,0,1)
                checkbox.active=False
                return None

            self.layout.ids.SampleTime.text=self.marg.getSampleTime()

            self.margOn = True
            self.layout.ids.MargStatus.text = "Connected"
            self.set_disable_all_children(self.layout.ids.Marg, False)
            self.layout.ids.MargStatus.color = (0,1,0, 1)
        else:
            self.layout.ids.MargStatus.text = "Disconnecting..."
            if self.margOn:
                self.layout.ids.MargStatus.color = (1, 1, 0, 1)
                self.marg.stop()
            self.margOn = False
            self.layout.ids.MargStatus.text = "Disconnected"
            self.set_disable_all_children(self.layout.ids.Marg, True)

    def set_disable_all_children(self,widget,value):
        nodisable = [v for k,v in self.layout.ids.items() if k.find("_nodisable")>=0]
        if isinstance(widget, Button) or isinstance(widget, CheckBox) or isinstance(widget, TextInput):
            widget.disabled=value
            if widget in nodisable:
                widget.disabled=False
        for child in widget.children:
            self.set_disable_all_children(child,value)

    def updateLabels(self,dt):
        if self.margOn:
            data = self.marg.monitorReadOne()
            st = str(self.marg.getSampleTime())
            self.layout.ids.ActualSampleTime.text = st
            if data is not None:
                margnames = ["AccX","AccY","AccZ","GyroX","GyroY","GyroZ","MagX","MagY","MagZ"]
                for name in margnames:
                    self.layout.ids[name].text = str(data[name])

        if self.mocapOn:
            name = self.layout.ids.TrackedBody.text
            if name != "None":
                self.mocap.setTrackedBody(name)
                data = self.mocap.monitorReadOne()
                if data is not None:
                    mocapnames = ["PosX", "PosY", "PosZ", "Q1", "Q2", "Q3","Q4","RX","RY","RZ"]
                    for i in range(len(mocapnames)):
                        self.layout.ids[mocapnames[i]].text = str(round(data[i],4))

    def SetSampleTime_callback(self, *args):
        try:
            sampletime = int(self.layout.ids.SampleTime.text)
            if sampletime>=4 and sampletime<=1000:
                self.marg.setSampleTime(sampletime)
        except:
            pass

    def LoadProgram_callback(self, *args):

        path = self.layout.ids.ConfigFile.text
        randData = {"Type": self.layout.ids.SpinnerNoise.text, "Param": [float(self.layout.ids.tanValue.text), float(self.layout.ids.normValue.text), float(self.layout.ids.binormValue.text)]}
        kineData = {"Type": self.layout.ids.SpinnerParam.text, "Min": [float(self.layout.ids.rMinVal.text), float(self.layout.ids.vMinVal.text), float(self.layout.ids.aMinVal.text)],
                    "Max": [float(self.layout.ids.rMaxVal.text), float(self.layout.ids.vMaxVal.text), float(self.layout.ids.aMaxVal.text)], "Epochnum": int(self.layout.ids.Epochs.text)}
        self.robot.loadProgram(path, randData, kineData)
        print("Load finished")


    def SpinnerParam_callback(self,spinner,text):
        _,disable,textmin,textmax=self.robot.paramStepperDict[text].getOptionInfo()
        self.set_disable_all_children(self.layout.ids.ParamMax,disable)
        self.layout.ids.ParamMinLabel.text=textmin
        self.layout.ids.ParamMaxLabel.text=textmax

    def SpinnerNoise_callback(self,spinner,text):
        _,textparam=self.robot.randomizerDict[text].getOptionInfo()
        self.layout.ids.ParamNoiseLabel.text=textparam

    def MoveRobot_callback(self,button):
        self.robot.startProgram()

    def StopRobot_callback(self,button):
        self.robot.stopProgram()

    def StartButton_callback(self,button):
        self.logger.prepareLogPath(self.layout.ids.PathInput.text)
        devices = {}
        if self.margOn:
            devices["Marg"]=self.marg
        if self.mocapOn:
            devices["Mocap"]=self.mocap
        if self.robotOn:
            self.robot.logProgram(self.logger.logpath)
        if len(devices.keys())>0:
            self.logger.setDevices(devices)
            self.logger.startLogging()
            self.layout.ids.LoggerStatus.text = "Logging"
            self.layout.ids.LoggerStatus.color = (1,1,0,1)
            self.loggerOn = True

    def StopButton_callback(self,button):
        self.logger.stopLogging()
        self.layout.ids.LoggerStatus.text = "Stopped"
        self.layout.ids.LoggerStatus.color = (1, 0, 0, 1)
        self.loggerOn=False

    def on_stop(self):
        if self.margOn:
            self.marg.stop()

        if self.mocapOn:
            self.mocap.stop()

        if self.robotOn:
            self.robot.stop()

        if self.loggerOn:
            self.logger.stopLogging()

if __name__ =='__main__':
    Logger_app(kv_file='MARG_mod.kv').run()
